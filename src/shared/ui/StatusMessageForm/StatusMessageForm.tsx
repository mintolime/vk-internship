import { Text } from "@vkontakte/vkui";
import { FC } from "react";

interface StatusMessageFormProps {
  message: string | number; // для разных форм (age & facts) разные типы
  status?: string;
}

export const StatusMessageForm: FC<StatusMessageFormProps> = ({
  message,
  status = "success",
}) => {
  return (
    <>
      {status === "error" ? (
        <Text
          weight="3"
          style={{ padding: "5px", color: "red", fontSize: "12px" }}
        >
          {message}
        </Text>
      ) : (
        <Text weight="3" style={{ padding: "5px" }}>
          {message}
        </Text>
      )}
    </>
  );
};
