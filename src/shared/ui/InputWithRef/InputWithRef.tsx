import { Input, InputProps } from "@vkontakte/vkui";
import { forwardRef } from "react";

interface InputWithRefProps extends InputProps {}

export const InputWithRef = forwardRef<HTMLInputElement, InputWithRefProps>(
  (props, ref) => {
    return <Input {...props} getRef={ref} />;
  },
);
