import { PanelHeader, SplitLayout } from "@vkontakte/vkui";
import { FC, useState } from "react";
import { Sidebar } from "../../../widgets/SideBar";
import { panels } from "../../../shared/const/const";
import { AppFormsUser } from "../../../widgets/AppFormsUser";

export const Main: FC = () => {
  const [activePanel, setActivePanel] = useState(panels[0]);

  return (
    <>
      <PanelHeader fixed>Forms</PanelHeader>
      <SplitLayout style={{ justifyContent: "center" }}>
        <Sidebar panelSidebar={activePanel} setPanelSidebar={setActivePanel} />
        <AppFormsUser panelForms={activePanel} />
      </SplitLayout>
    </>
  );
};
