import { AppRoot } from "@vkontakte/vkui";
import "@vkontakte/vkui/dist/vkui.css";
import React from "react";
import { Main } from "../pages/Main";
function App() {
  return (
    <AppRoot>
      <Main />
    </AppRoot>
  );
}

export default App;
