import React from "react";
import ReactDOM from "react-dom/client";
import "../src/app/styles/index.css";
import App from "./app/App";
import { AdaptivityProvider, ConfigProvider } from "@vkontakte/vkui";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <ConfigProvider>
    <AdaptivityProvider>
      <App />
    </AdaptivityProvider>
  </ConfigProvider>,
);
