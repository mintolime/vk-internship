import { Panel, SplitCol, View } from "@vkontakte/vkui";
import React, { FC } from "react";

import { panels } from "../../../shared/const/const";
import { ShowUserAgeForm } from "../../../features/ShowUserAgeForm";
import { CatsFactsForm } from "../../../features/CatsFactsForm";

interface AppFormsUserProps {
  panelForms: string;
}

export const AppFormsUser: FC<AppFormsUserProps> = (props) => {
  const { panelForms } = props;
  return (
    <SplitCol width="100%" maxWidth="560px" stretchedOnMobile autoSpaced>
      <View activePanel={panelForms}>
        <Panel nav={panels[0]}>
          <CatsFactsForm />
        </Panel>
        <Panel nav={panels[1]}>
          <ShowUserAgeForm />
        </Panel>
      </View>
    </SplitCol>
  );
};
