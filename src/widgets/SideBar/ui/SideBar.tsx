import { Cell, Group, Panel, SplitCol } from "@vkontakte/vkui";
import { panels } from "../../../shared/const/const";
import { FC } from "react";
import { useAdaptivityConditionalRender } from "@vkontakte/vkui";

interface SidebarProps {
  panelSidebar: string;
  // тип для стейта
  setPanelSidebar: React.Dispatch<React.SetStateAction<string>>;
}

export const Sidebar: FC<SidebarProps> = ({
  panelSidebar,
  setPanelSidebar,
}) => {
  const { viewWidth } = useAdaptivityConditionalRender();

  return viewWidth.tabletPlus ? (
    <SplitCol
      className={viewWidth.tabletPlus.className}
      width={280}
      maxWidth={280}
    >
      <Panel>
        <Group>
          {panels.map((i) => (
            <Cell
              key={i}
              hovered={i === panelSidebar}
              onClick={() => setPanelSidebar(i)}
            >
              {i}
            </Cell>
          ))}
        </Group>
      </Panel>
    </SplitCol>
  ) : null;
};
