import React, { useRef, useState } from "react";
import axios from "axios";
import { Controller, useForm } from "react-hook-form";
import { Group, Header } from "@vkontakte/vkui";
import { AppForm } from "../../../entities/AppForm";
import { InputWithRef } from "../../../shared/ui/InputWithRef/InputWithRef";
import { StatusMessageForm } from "../../../shared/ui/StatusMessageForm/StatusMessageForm";

export const CatsFactsForm = () => {
  // стейт для форм react-hook-form
  const {
    control: controlCats,
    handleSubmit: handleSubmitCats,
    reset: resetCats,
  } = useForm({
    defaultValues: {
      factsСats: "Факты,и ничего кроме фактов",
    },
  });
  const [loading, setLoading] = useState(false);
  const inputRef = useRef(null);
  const [errorResponse, setErrorResponse] = useState("");

  const onSubmitFormsCats = async (data: { factsСats: string }) => {
    try {
      setLoading(true);
      const response = await axios.get("https://catfact.ninja/fact");
      if (response && response.data && response.data.fact) {
        data.factsСats = response.data.fact;
        resetCats(data);
      }
    } catch (error) {
      setErrorResponse(error.message);
      console.error("Ошибка запроса:", error.message);
    } finally {
      setLoading(false);
    }
  };

  const setCursorAfterFirstWord = () => {
    if (inputRef?.current) {
      const text = inputRef?.current.value;
      const firstSpaceIndex = text.indexOf(" ");
      if (firstSpaceIndex > 0) {
        inputRef?.current.setSelectionRange(
          firstSpaceIndex + 1,
          firstSpaceIndex + 2,
        );
        inputRef.current.focus();
      }
    }
  };

  // const handleSelect = (event) => {
  //   console.log(
  //     'Selected text:',
  //     event.target.value.substring(event.target.selectionStart, event.target.selectionEnd),
  //   );
  // };

  return (
    <Group header={<Header mode="secondary">Рандомный факт про кошек</Header>}>
      <AppForm
        htmlForId="facts-cats"
        onSubmit={handleSubmitCats(onSubmitFormsCats)}
        btnText={"Узнать"}
        isLoading={loading}
        disabledBtn={loading}
      >
        <Controller
          name="factsСats"
          control={controlCats}
          render={({ field }) => (
            <>
              <InputWithRef
                {...field}
                id="factsСats"
                type="text"
                disabled={loading}
                ref={inputRef}
                onFocus={setCursorAfterFirstWord}
                // onSelect={handleSelect}
              />
              {/* т.к мы не передаем параметры при запросе, но ошибки с api все равно могут быть */}
              {errorResponse && (
                <StatusMessageForm message={errorResponse} status="error" />
              )}
            </>
          )}
        />
      </AppForm>
    </Group>
  );
};
