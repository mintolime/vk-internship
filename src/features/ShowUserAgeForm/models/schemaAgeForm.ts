import * as yup from "yup";

export const schemaAgeForm = yup.object().shape({
  ageUserForm: yup
    .string()
    .required("Поле обязательно для заполнения")
    .matches(/^[a-zA-Z]+$/, "Только буквы на английском")
    .trim(),
});
