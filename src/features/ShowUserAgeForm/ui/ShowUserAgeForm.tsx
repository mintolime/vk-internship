/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import axios from "axios";
import { Icon16Clear } from "@vkontakte/icons";
import { yupResolver } from "@hookform/resolvers/yup";
import { Controller, useForm } from "react-hook-form";
import { Group, Header, IconButton } from "@vkontakte/vkui";

import { AppForm } from "../../../entities/AppForm";
import { InputWithRef } from "../../../shared/ui/InputWithRef/InputWithRef";
import { StatusMessageForm } from "../../../shared/ui/StatusMessageForm/StatusMessageForm";
import { schemaAgeForm } from "../models/schemaAgeForm";

export const ShowUserAgeForm = () => {
  // стейт для форм react-hook-form
  const {
    control: controlAge,
    handleSubmit: handleSubmitAge,
    reset: resetAge,
    watch,
    trigger,
    formState: { errors: errorsAge },
  } = useForm({
    defaultValues: {
      ageUserForm: "",
    },
    resolver: yupResolver(schemaAgeForm),
  });

  const [previousName, setPreviousName] = useState(null);
  const [loading, setLoading] = useState(false);
  const [userAgeResult, setUserAgeResult] = useState(0);
  const [errorResponse, setErrorResponse] = useState("");
  const ageUserFormValue = watch("ageUserForm");

  useEffect(() => {
    // тип, который представляет идентификатор таймера, возвращаемый функцией setTimeout()
    let timerId: NodeJS.Timeout;

    // задаем условие чтобы при ошибке, не отправлялся запрос к api
    if (
      ageUserFormValue &&
      ageUserFormValue.length >= 3 &&
      !errorsAge.ageUserForm
    ) {
      timerId = setTimeout(() => {
        onSubmitFormsAge({ ageUserForm: ageUserFormValue });
      }, 3000);
    }
    // данный метод вызывает валидацию формы, также чтобы триггер работал только на ввод
    ageUserFormValue && trigger("ageUserForm");
    // очистка счетчика
    return () => clearTimeout(timerId);
  }, [ageUserFormValue]);

  const onSubmitFormsAge = async (data: { ageUserForm: string }) => {
    setErrorResponse("");
    if (loading) return; // если запрос уже есть и он загружается, не отправляем новый

    try {
      setLoading(true); // предотвращаем отправку новых запросов и запускаем загрузку

      if (data.ageUserForm !== previousName) {
        const response = await axios.get(
          `https://api.agify.io/?name=${data.ageUserForm}`,
        );
        if (response.data && response.data.age) {
          setUserAgeResult(response.data.age);
        }
        setPreviousName(data.ageUserForm);
      } else {
        setErrorResponse("Запрос с тем же именем уже отправлен.");
      }
    } catch (error) {
      setErrorResponse(error.message);
      console.error(
        "Ошибка при получении возраста пользователя:",
        error.message,
      );
    } finally {
      setLoading(false); // После завершения запроса сбрасываем загрузку
    }
  };

  const handleClearButtonClick = () => {
    resetAge();
    setUserAgeResult(0);
    setErrorResponse("");
  };

  return (
    <Group
      header={
        <Header mode="secondary">Какой твой психологический возраст?</Header>
      }
    >
      <AppForm
        htmlForId="ageUserForm"
        onSubmit={handleSubmitAge(onSubmitFormsAge)}
        btnText="Проверить"
        isLoading={loading}
        disabledBtn={loading}
      >
        <Controller
          name="ageUserForm"
          control={controlAge}
          render={({ field }) => (
            <>
              <InputWithRef
                {...field}
                id="ageUserForm"
                type="text"
                placeholder="Ваше имя"
                after={
                  <IconButton
                    hoverMode="opacity"
                    label="Очистить поле"
                    onClick={() => handleClearButtonClick()}
                  >
                    <Icon16Clear />
                  </IconButton>
                }
              />
              {errorsAge.ageUserForm && (
                <StatusMessageForm
                  message={errorsAge.ageUserForm.message}
                  status="error"
                />
              )}
              {errorResponse && (
                <StatusMessageForm message={errorResponse} status="error" />
              )}
            </>
          )}
        />
        {/* результат запроса */}
        {!errorsAge.ageUserForm && (
          <StatusMessageForm message={userAgeResult} />
        )}
      </AppForm>
    </Group>
  );
};
