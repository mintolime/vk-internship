import {
  FormLayoutGroup,
  FormItem,
  Button,
  ButtonProps,
} from "@vkontakte/vkui";
import { FC, ReactNode, FormEvent } from "react";

interface AppFormProps {
  children: ReactNode;
  htmlForId?: string;
  modeForm?: "vertical" | "horizontal";
  btnText: string;
  isLoading?: boolean;
  disabledBtn?: boolean;
  onSubmit: (e: FormEvent) => void;
}

export const AppForm: FC<AppFormProps> = (props) => {
  const {
    htmlForId,
    children,
    modeForm,
    onSubmit,
    btnText,
    isLoading,
    disabledBtn,
  } = props;

  return (
    <FormLayoutGroup mode={modeForm} onSubmit={onSubmit}>
      <FormItem htmlFor={htmlForId}>{children}</FormItem>
      <Button
        type="submit"
        size="s"
        mode="outline"
        style={{ margin: "0 16px" }}
        disabled={disabledBtn}
        onClick={onSubmit}
        loading={isLoading}
      >
        {btnText}
      </Button>
    </FormLayoutGroup>
  );
};
